package com.example.expensetracker.ui.main

import android.os.Bundle
import com.example.expensetracker.base.activity.BaseActivity
import com.example.expensetracker.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding>({ActivityMainBinding.inflate(it)}) {
  override fun onActivityCreated(savedInstanceState: Bundle?) {

  }
}
package com.example.expensetracker.ui

import androidx.lifecycle.ViewModel
import com.example.expensetracker.extension.SingleLiveData
import com.example.expensetracker.extension.addToCompositeDisposable
import com.example.expensetracker.extension.applyIO
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import javax.inject.Inject

sealed class ViewModelState {
  object Idle : ViewModelState()
  data class Loading(var blockUI: Boolean) : ViewModelState()
  data class Error(val code: String? = null, val error: String) : ViewModelState()
}

@HiltViewModel
open class BaseViewModel @Inject constructor() : ViewModel() {

  val loadingStateLiveData = SingleLiveData<ViewModelState?>()

  protected val compositeDisposable = CompositeDisposable()

  open fun showProgress(shouldBlockUI: Boolean) {
    loadingStateLiveData.postValue(ViewModelState.Loading(shouldBlockUI))
  }

  open fun hideProgress() {
    loadingStateLiveData.postValue(ViewModelState.Idle)
  }

  private fun disposeAll() {
    compositeDisposable.clear()
    RxJavaPlugins.setErrorHandler(null)
  }

  private fun <T> callApi(
    observable: Observable<T>,
    withProgress: Boolean = true,
    shouldBlockUI: Boolean = true,
    onApiError: ((ViewModelState.Error) -> Unit)? = null
  ): Observable<T> = observable
    .applyIO()
    .doOnSubscribe { if (withProgress) showProgress(shouldBlockUI) }
    .doOnComplete { hideProgress() }
    .doOnError { handleError(it).let { onApiError?.invoke(it) } }
    .doOnDispose { hideProgress() }

  private fun Completable.callApi(
    withProgress: Boolean = true,
    shouldBlockUI: Boolean = true,
    onApiError: ((ViewModelState.Error) -> Unit)? = null
  ): Completable = this
    .applyIO()
    .doOnSubscribe { if (withProgress) showProgress(shouldBlockUI) }
    .doOnComplete { hideProgress() }
    .doOnError { handleError(it).let { onApiError?.invoke(it) } }
    .doOnDispose { hideProgress() }

  private fun <T> Single<T>.callApi(
    withProgress: Boolean = true,
    shouldBlockUI: Boolean = true,
    onApiError: ((ViewModelState.Error) -> Unit)? = null
  ): Single<T> = this
    .applyIO()
    .doOnSubscribe { if (withProgress) showProgress(shouldBlockUI) }
    .doOnSuccess { hideProgress() }
    .doOnError { handleError(it).let { onApiError?.invoke(it) } }
    .doOnDispose { hideProgress() }

  protected fun <T> justSubscribe(
    observable: Observable<T>,
    withProgress: Boolean = true,
    shouldBlockUI: Boolean = true,
    onApiError: ((ViewModelState.Error) -> Unit)? = null
  ) = callApi(observable, withProgress, shouldBlockUI, onApiError)
    .subscribe({}, { it.printStackTrace() })
    .addToCompositeDisposable(compositeDisposable)

  protected fun justSubscribe(
    completable: Completable,
    withProgress: Boolean = true,
    shouldBlockUI: Boolean = true,
    onApiError: ((ViewModelState.Error) -> Unit)? = null
  ) = completable.callApi(withProgress, shouldBlockUI, onApiError)
    .subscribe({}, { it.printStackTrace() })
    .addToCompositeDisposable(compositeDisposable)

  protected fun <T> justSubscribe(
    single: Single<T>,
    withProgress: Boolean = true,
    shouldBlockUI: Boolean = true,
    onApiError: ((ViewModelState.Error) -> Unit)? = null
  ) = single.callApi(withProgress, shouldBlockUI, onApiError)
    .subscribe({}, { it.printStackTrace() })
    .addToCompositeDisposable(compositeDisposable)

  override fun onCleared() {
    disposeAll()
    super.onCleared()
  }

  private fun handleError(throwable: Throwable): ViewModelState.Error {
    hideProgress()
    val error = ViewModelState.Error(error = throwable.localizedMessage)
    loadingStateLiveData.postValue(error)
    return error
  }
}
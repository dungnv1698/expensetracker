package com.example.expensetracker.extension

import com.example.expensetracker.BuildConfig

object SharedPrefsSettingsConstant {
  const val SHARED_PREFS_NAME = "${BuildConfig.APPLICATION_ID}_PREFS_SETTINGS"

  // Define keys
  const val INSERTED_STUB_DATA = "INSERTED_STUB_DATA"
}
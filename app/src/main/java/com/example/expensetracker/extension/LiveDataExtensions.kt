package com.example.expensetracker.extension

import androidx.annotation.MainThread
import androidx.lifecycle.*

/**
 * Observe not null
 *
 * @param T
 * @param owner
 * @param observer
 * @receiver
 */
fun <T> LiveData<T>.observeNotNull(owner: LifecycleOwner, observer: (t: T) -> Unit) {
  if (!this.hasObservers()) {
    this.observe(owner, Observer { it?.let(observer) })
  }
}

/**
 * Observe null
 *
 * @param T
 * @param owner
 * @param observer
 * @receiver
 */
fun <T> LiveData<T>.observeNull(owner: LifecycleOwner, observer: (t: T?) -> Unit) {
  this.observe(owner, Observer { observer.invoke(it) })
}

/**
 * Distinct until changed
 *
 * @param X
 * @param comparer
 * @receiver
 * @return
 */
@MainThread
fun <X> LiveData<X>.distinctUntilChanged(comparer: (X, X) -> Boolean): LiveData<X> {
  val result = MediatorLiveData<X>()

  result.addSource(this) {
    it?.let {
      val currentValue = result.value
      if (currentValue != null) {
        if (!comparer(it, currentValue)) {
          result.value = it
        }
      } else {
        result.value = it
      }
    }
  }

  return result
}

/**
 * Distinct until changed
 *
 * @param X
 * @return
 */
@MainThread
fun <X> LiveData<X>.distinctUntilChanged(): LiveData<X> {
  return distinctUntilChanged { v1, v2 ->
    v1 == v2
  }
}
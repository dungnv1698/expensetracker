package com.example.expensetracker.extension

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.CompletableEmitter
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.MaybeEmitter
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

fun Disposable.addToCompositeDisposable(compositeDisposable: CompositeDisposable) {
  if (!compositeDisposable.isDisposed) {
    compositeDisposable.add(this)
  }
}

fun <T> Observable<T>.applyIO(): Observable<T> {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.applyComputation(): Observable<T> {
  return this
    .subscribeOn(Schedulers.computation())
    .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Single<T>.applyIO(): Single<T> {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Single<T>.applyComputation(): Single<T> {
  return this
    .subscribeOn(Schedulers.computation())
    .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.toMainThread(): Observable<T> {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Single<T>.toMainThread(): Single<T> {
  return this
    .observeOn(AndroidSchedulers.mainThread())
}

fun Completable.toMainThread(): Completable {
  return this
    .observeOn(AndroidSchedulers.mainThread())
}

fun Completable.applyIO(): Completable {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
}

fun Completable.applyComputation(): Completable {
  return this
    .subscribeOn(Schedulers.computation())
    .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Maybe<T>.applyIO(): Maybe<T> {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Flowable<T>.applyIO(): Flowable<T> {
  return this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
}

fun <T> ObservableEmitter<T>.checkDisposed(): ObservableEmitter<T>? {
  return if (this.isDisposed) {
    null
  } else {
    this
  }
}

fun <T> SingleEmitter<T>.checkDisposed(): SingleEmitter<T>? {
  return if (this.isDisposed) {
    null
  } else {
    this
  }
}

fun <T> CompletableEmitter.checkDisposed(): CompletableEmitter? {
  return if (this.isDisposed) {
    null
  } else {
    this
  }
}

fun <T> MaybeEmitter<T>.checkDisposed(): MaybeEmitter<T>? {
  return if (this.isDisposed) {
    null
  } else {
    this
  }
}
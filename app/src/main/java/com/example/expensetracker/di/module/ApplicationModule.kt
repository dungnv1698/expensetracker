package com.example.expensetracker.di.module

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.example.expensetracker.data.db.DatabaseConstants
import com.example.expensetracker.data.db.ExpenseTrackerDatabase
import com.example.expensetracker.extension.SharedPrefsSettingsConstant
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ApplicationModule {
  @Provides
  @Singleton
  fun provideDatabase(@ApplicationContext context: Context): ExpenseTrackerDatabase =
    Room.databaseBuilder(
      context,
      ExpenseTrackerDatabase::class.java,
      DatabaseConstants.DATABASE_NAME
    ).build()

  @Provides
  @Singleton
  fun provideSharePreference(@ApplicationContext context: Context): SharedPreferences {
    return context.getSharedPreferences(
      SharedPrefsSettingsConstant.SHARED_PREFS_NAME,
      Context.MODE_PRIVATE
    )
  }

  @Provides
  @Singleton
  fun provideCategoryDao(expenseTrackerDatabase: ExpenseTrackerDatabase) =
    expenseTrackerDatabase.categoryDao()

  @Provides
  @Singleton
  fun provideExpenseDao(expenseTrackerDatabase: ExpenseTrackerDatabase) =
    expenseTrackerDatabase.expenseDao()
}
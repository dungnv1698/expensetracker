package com.example.expensetracker.base.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.viewbinding.ViewBinding

abstract class BaseActivity<VB : ViewBinding>(val bindingFactory: (LayoutInflater) -> VB) :
  HiltActivity() {

  private var _viewBinding: VB? = null

  abstract fun onActivityCreated(savedInstanceState: Bundle?)

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    _viewBinding = bindingFactory(layoutInflater)
    setContentView(viewBinding().root)

    if (shouldShowFullscreen()) {
      makeFullScreen()
    }
    onActivityCreated(savedInstanceState)
  }

  override fun onDestroy() {
    super.onDestroy()
    _viewBinding = null
  }

  private fun makeFullScreen() {
    this.window.decorView.systemUiVisibility =
      View.SYSTEM_UI_FLAG_IMMERSIVE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
  }

  fun viewBinding(): VB {
    if (_viewBinding == null) throw NullPointerException("viewBinding is null!!")
    return _viewBinding!!
  }

  open fun shouldShowFullscreen(): Boolean {
    return false
  }
}
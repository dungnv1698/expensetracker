package com.example.expensetracker.base.fragment

import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
abstract class HiltFragment : Fragment() {

}
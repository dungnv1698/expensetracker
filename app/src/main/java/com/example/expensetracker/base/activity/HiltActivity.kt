package com.example.expensetracker.base.activity

import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
open class HiltActivity : AppCompatActivity() {

}
package com.example.expensetracker.base.fragment

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.example.expensetracker.extension.observeNotNull
import com.example.expensetracker.ui.BaseViewModel
import com.example.expensetracker.ui.ViewModelState
import timber.log.Timber

abstract class BaseFragment<VB : ViewBinding>(val bindingFactory: (LayoutInflater) -> VB) :
    HiltFragment() {

    private var _viewBinding: VB? = null
    private val binding: VB
        get() {
            if (_viewBinding == null) throw NullPointerException("viewBinding is null!!")
            return _viewBinding!!
        }

    protected val progressDialog: ProgressDialog by lazy { ProgressDialog(requireContext()) }

    private val backPressCallback: OnBackPressedCallback by lazy {
        return@lazy object : OnBackPressedCallback(shouldHandleOnBackPressed()) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _viewBinding = bindingFactory(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel().loadingStateLiveData.observeNotNull(viewLifecycleOwner) {
            when (it) {
                is ViewModelState.Loading -> {
                        progressDialog.show()
                }
                is ViewModelState.Idle -> {
                    progressDialog.hide()
                }
                is ViewModelState.Error -> {
                    progressDialog.hide()
                    displayError(it)
                }
                else -> {}
            }
        }
        onFragmentCreated(view, savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        setupOnBackPressed()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
        progressDialog.hide()
    }

    abstract fun viewModel(): BaseViewModel

    abstract fun onFragmentCreated(view: View, savedInstanceState: Bundle?)

    open fun displayError(error: ViewModelState.Error) {
        Timber.e("displayError: $error")
    }

    open fun shouldHandleOnBackPressed(): Boolean {
        return true
    }

    private fun setupOnBackPressed() {
        activity?.onBackPressedDispatcher?.addCallback(this, backPressCallback)
    }

    open fun onBackPressed() {
        if (!findNavController().popBackStack()) {
            activity?.finish()
        }
    }
}
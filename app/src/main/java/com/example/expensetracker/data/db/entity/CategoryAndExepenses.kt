package com.example.expensetracker.data.db.entity

import androidx.room.Embedded
import androidx.room.Relation

data class CategoryAndExpenses(
  @Embedded val category: Category,
  @Relation(
    parentColumn = "id",
    entityColumn = "categoryId"
  )
  val expenses: List<Expense>
)
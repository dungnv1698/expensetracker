package com.example.expensetracker.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.expensetracker.data.db.DatabaseConstants
import com.example.expensetracker.data.db.entity.Category
import com.example.expensetracker.data.db.entity.CategoryAndExpenses
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

@Dao
interface CategoryDao {
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insertCategories(categories: List<Category>): Completable

  @Query("SELECT * from ${DatabaseConstants.TABLE_CATEGORY}")
  fun getCategories(): Observable<List<Category>>

  @Query("SELECT * FROM ${DatabaseConstants.TABLE_CATEGORY}")
  fun getCategoryAndExpenses(): Single<List<CategoryAndExpenses>>
}
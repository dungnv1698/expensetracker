package com.example.expensetracker.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.expensetracker.data.db.DatabaseConstants

@Entity(tableName = DatabaseConstants.TABLE_CATEGORY)
data class Category(
  @PrimaryKey(autoGenerate = true)
  @ColumnInfo(name = DatabaseConstants.COLUMN_ID)
  val id: Long,
  @ColumnInfo(name = DatabaseConstants.COLUMN_CATEGORY_NAME)
  val categoryName: String
) {
  annotation class CategoryType {
    companion object {
      const val SHOPPING = "Shopping"
      const val VEHICLE = "Vehicle"
      const val FOOD = "Food"
    }
  }
}
package com.example.expensetracker.data.repository

import android.content.SharedPreferences
import com.example.expensetracker.data.db.dao.CategoryDao
import com.example.expensetracker.data.db.dao.ExpenseDao
import com.example.expensetracker.data.db.entity.Category
import com.example.expensetracker.data.db.entity.Category.CategoryType
import com.example.expensetracker.extension.SharedPrefsSettingsConstant
import io.reactivex.rxjava3.core.Completable
import javax.inject.Inject

class FakeDataRepository @Inject constructor(
  private val sharedPreferences: SharedPreferences,
  private val categoryDao: CategoryDao,
  private val expenseDao: ExpenseDao
) : IFakeDataRepository {
  override fun fakeDate(): Completable {
    if (sharedPreferences.getBoolean(SharedPrefsSettingsConstant.INSERTED_STUB_DATA, false)) {
      return Completable.complete()
    }

    return fakeCategories().andThen(fakeExpenses()).andThen(Completable.fromAction {
      sharedPreferences.edit().putBoolean(SharedPrefsSettingsConstant.INSERTED_STUB_DATA, true)
        .apply()
    })
  }

  override fun fakeCategories(): Completable {
    val categories = listOf(
      Category(1, CategoryType.SHOPPING),
      Category(2, CategoryType.VEHICLE),
      Category(3, CategoryType.FOOD)
    )

    return categoryDao.insertCategories(categories)
  }

  override fun fakeExpenses(): Completable {
    // TODO: Insert expense later
    return expenseDao.insert(listOf())
  }

}
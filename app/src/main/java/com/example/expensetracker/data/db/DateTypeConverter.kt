package com.example.expensetracker.data.db

import androidx.room.TypeConverter
import java.util.Date

object DateTypeConverter {
  @TypeConverter
  @JvmStatic
  fun toDate(date: Long): Date = Date(date)

  @TypeConverter
  @JvmStatic
  fun toLong(date: Date): Long = date.time
}
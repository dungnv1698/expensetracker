package com.example.expensetracker.data.db

object DatabaseConstants {
  const val DATABASE_NAME = "expense-tracker"
  const val DATABASE_VERSION = 1

  const val COLUMN_ID = "id"

  // Category
  const val TABLE_CATEGORY = "category"
  const val COLUMN_CATEGORY_NAME = "categoryName"

  // Expense
  const val TABLE_EXPENSE = "expense"
  const val COLUMN_ = "notificationId"
  const val COLUMN_EXPENSE_ACCOUNT = "account"
  const val COLUMN_EXPENSE_CATEGORYID = "categoryId"
  const val COLUMN_EXPENSE_DESCRIPTION = "description"
  const val COLUMN_EXPENSE_AMOUNT = "amount"
  const val COLUMN_EXPENSE_ISEARNING = "isEarning"
  const val COLUMN_EXPENSE_DATE = "date"
}
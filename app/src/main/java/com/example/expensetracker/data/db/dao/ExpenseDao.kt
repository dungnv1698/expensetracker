package com.example.expensetracker.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.expensetracker.data.db.DatabaseConstants
import com.example.expensetracker.data.db.entity.Expense
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import java.util.Date

@Dao
interface ExpenseDao {
  @Insert
  fun insert(expenses: List<Expense>): Completable

  @Insert
  fun insert(expense: Expense): Completable

  @Query("SELECT * FROM ${DatabaseConstants.TABLE_EXPENSE} ORDER BY ${DatabaseConstants.COLUMN_EXPENSE_DATE}")
  fun getExpenses(): Observable<List<Expense>>

  @Query(
    "SELECT * FROM ${DatabaseConstants.TABLE_EXPENSE} WHERE :startDate <= ${DatabaseConstants.COLUMN_EXPENSE_DATE} AND ${DatabaseConstants.COLUMN_EXPENSE_DATE} <= :endDate" +
        " ORDER BY ${DatabaseConstants.COLUMN_EXPENSE_DATE}"
  )
  fun getExpenses(startDate: Date, endDate: Date): Observable<List<Expense>>
}
package com.example.expensetracker.data.repository

import io.reactivex.rxjava3.core.Completable

interface IFakeDataRepository {
  fun fakeDate(): Completable
  fun fakeCategories(): Completable
  fun fakeExpenses(): Completable
}
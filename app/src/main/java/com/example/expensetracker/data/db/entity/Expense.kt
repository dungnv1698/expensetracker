package com.example.expensetracker.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.example.expensetracker.data.db.DatabaseConstants
import java.util.Date

@Entity(
  tableName = DatabaseConstants.TABLE_EXPENSE,
  foreignKeys = [
    ForeignKey(
      entity = Category::class,
      parentColumns = [DatabaseConstants.COLUMN_ID],
      childColumns = [DatabaseConstants.COLUMN_EXPENSE_CATEGORYID],
      onDelete = ForeignKey.CASCADE
    )
  ]
)
data class Expense(
  @PrimaryKey(autoGenerate = true)
  @ColumnInfo(name = DatabaseConstants.COLUMN_ID)
  val id: Long,
  @ColumnInfo(name = DatabaseConstants.COLUMN_EXPENSE_ACCOUNT)
  val account: String,
  @ColumnInfo(name = DatabaseConstants.COLUMN_EXPENSE_CATEGORYID, index = true)
  val categoryId: Long,
  @ColumnInfo(name = DatabaseConstants.COLUMN_EXPENSE_DESCRIPTION)
  val description: String?,
  @ColumnInfo(name = DatabaseConstants.COLUMN_EXPENSE_AMOUNT)
  val amount: Double,
  @ColumnInfo(name = DatabaseConstants.COLUMN_EXPENSE_ISEARNING)
  val isEarning: Boolean,
  @ColumnInfo(name = DatabaseConstants.COLUMN_EXPENSE_DATE)
  val date: Date
)
package com.example.expensetracker.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.expensetracker.data.db.dao.CategoryDao
import com.example.expensetracker.data.db.dao.ExpenseDao
import com.example.expensetracker.data.db.entity.Category
import com.example.expensetracker.data.db.entity.Expense

@Database(
  entities = [Category::class, Expense::class],
  version = DatabaseConstants.DATABASE_VERSION
)
@TypeConverters(DateTypeConverter::class)
abstract class ExpenseTrackerDatabase() : RoomDatabase() {
  abstract fun categoryDao(): CategoryDao
  abstract fun expenseDao(): ExpenseDao
}
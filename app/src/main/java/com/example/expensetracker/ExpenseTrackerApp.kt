package com.example.expensetracker

import android.app.Application
import android.content.Context
import com.example.expensetracker.data.repository.FakeDataRepository
import com.example.expensetracker.extension.applyIO
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class ExpenseTrackerApp : Application() {
  @Inject
  lateinit var fakeDataRepository: FakeDataRepository

  override fun onCreate() {
    super.onCreate()
    application = this
    fakeDataRepository.fakeDate().applyIO().subscribe()
  }

  companion object {
    var application: Application? = null
      private set
    val context: Context
      get() = application!!.applicationContext
  }
}